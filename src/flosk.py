from flask import Flask, request

app = Flask(__name__)

@app.route('/', methods=['POST']) #Methodを明示する必要あり
# @app.route("/")
def hello():
    print(request.data)
    return request.json["Name"]

## おまじない
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)